const requireDir = require('require-dir');

module.exports = (gulp) => {
    require('./src/build')(gulp);
    require('./src/deploy')(gulp);
    require('./src/release')(gulp);
    require('./src/browserSync')(gulp);
    require('./src/default')(gulp);
}
