/**
 * Created by Superkrypt.
 * (2016)
 */

module.exports = (gulp) => {

	require('./build')(gulp);

	const fs = require('fs');
	const git = require('gulp-git');
	const bump = require('gulp-bump');
	const prompt = require('gulp-prompt');
	const replace = require('gulp-replace');
	const del = require('del');
	const environments = require( 'gulp-environments' );

	let packagejson = JSON.parse(fs.readFileSync('./package.json'));
	const globals = require('./globals');

	gulp.task('release:clean', (callback) => {

		return del([`./release/${packagejson.name}-${packagejson.version}`], callback);

	});

	gulp.task('release:create', gulp.series(
		(done) => { environments.current(environments.production); done(); },
		'release:clean',
		'build',
		() => {
			const destination = gulp.dest(`./release/${packagejson.name}-${packagejson.version}`);

			return gulp.src([
				`${globals.buildPath}/**/*`,
			])
				.pipe(replace('$VERSION$', packagejson.version, {
					skipBinary: true,
				}))
				.pipe((destination));

		})
	);

	let bumpType = 'to be changed';

	gulp.task('release:prompt-version', done => {

		return gulp.src('./package.json', { read: false })
			.pipe(prompt.prompt({
				type: 'list',
				name: 'bump',
				message: `What type of version bump would you like to do (last version: ${packagejson.version}?`,
				choices: ['patch', 'minor', 'major'],
			}, (res) => {

                bumpType = res.bump;

			}));

	});

	gulp.task('release:bump-version', gulp.series(
		'release:prompt-version',
		() => {
			return gulp.src('./package.json')
				.pipe(bump({ type: bumpType }))
				.pipe(gulp.dest('./'))
				.on('end', () => {
					packagejson = JSON.parse(fs.readFileSync('./package.json'));
				});
		},
	));

	gulp.task('release:to-git', gulp.parallel(
		() => gulp.src('./package.json').pipe(git.commit(`release version: ${packagejson.version}`)),
		() => git.tag(`v${packagejson.version}`, 'Release created'),
		)
	);

	gulp.task('release', gulp.series(
		'release:bump-version',
		'release:create',
		'release:to-git',
	));
}
