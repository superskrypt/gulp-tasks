const environments = require( 'gulp-environments' );
const { development, production } = environments;
require('dotenv').config();
const glob = require( 'glob' );

const Globals = {
	get buildPath() {
		return development() ? process.env.BUILD_PATH ? process.env.BUILD_PATH : 'build' : 'dist';
	},
	get assetsPath() {
		return this.buildPath + "/" + (process.env.ASSETS_PATH ? process.env.ASSETS_PATH : '');
	},
	get faviconPath() {
		return process.env.FAVICON_PATH;
	},
	get isScriptFileExist() {
		return glob.sync( './src/js/*.js' ).length;
	},
};

module.exports = Globals;
