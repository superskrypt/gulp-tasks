module.exports = (gulp) => {
    const webpack = require('webpack');
	const webpackDevMiddleware = require('webpack-dev-middleware');
	const webpackConfig = require('./build/webpack.config');

    const browserSync = require('browser-sync').create();

    const globals = require('./globals');

	gulp.task('browserSync:init', gulp.series(
		'build',
		() => {
			webpackConfig.watch = true;

			var options = {};

			if (globals.isScriptFileExist) {
				const bundler = webpack(webpackConfig);
				var options = {
					middleware: [
						webpackDevMiddleware(bundler, {
							publicPath: '/js/'
						})
					]
				}

			}

			if (process.env.DEV_PROXY) {
				options.proxy = process.env.DEV_PROXY;
			} else {
				options.server = globals.buildPath;
            }

            options.browser = process.env.BROWSER ? process.env.BROWSER : "google chrome";

			browserSync.init(options);

			gulp.watch('src/html/**/*', runAndReload('build:html'));
			gulp.watch('src/html-twig/**/*', runAndReload('build:html:twig'));
			gulp.watch('src/theme/**/*', runAndReload('build:theme'));
			gulp.watch('src/wp-plugin/**/*', runAndReload('build:plugin'));
			gulp.watch('src/scss/**/*', runAndReload('build:styles'));
			gulp.watch('src/js/**/*.js', runAndReload('build:scripts'));
			gulp.watch('src/images/**', runAndReload('build:images'));
			gulp.watch('src/fonts/**', runAndReload('build:fonts'));
			gulp.watch('src/svg/icons/*.svg', runAndReload('build:icons'));

        })
	);
	
	function runAndReload(taskName) {
		return gulp.series(taskName, done => {browserSync.reload(); done();});
	}
}
