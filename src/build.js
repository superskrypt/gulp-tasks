module.exports = (gulp) => {
	require('./build/favicons')(gulp);
	require('./build/html')(gulp);
	require('./build/theme')(gulp);
	require('./build/plugin')(gulp);
	require('./build/icons')(gulp);
    require('./build/images')(gulp);
    require('./build/scripts')(gulp);
    require('./build/styles')(gulp);
    require('./build/fonts')(gulp);

	const gulpLoadPlugins = require( 'gulp-load-plugins' );
	const del = require( 'del' );
	const globals = require( './globals' );

	const $ = gulpLoadPlugins();

	gulp.task('build:clean', cb => {

		return del([globals.buildPath], {force: true}, cb);

	});

	gulp.task('build:create', gulp.parallel(
		'build:styles',
		'build:icons',
		'build:scripts',
		'build:images',
		'build:html',
		'build:html:twig',
		'build:theme',
		'build:plugin',
		'build:fonts',
	));

	gulp.task('build', gulp.series(
		'build:clean',
		gulp.parallel(
			'build:theme:plugins',
			'build:create',
			)
		),
	);
}
