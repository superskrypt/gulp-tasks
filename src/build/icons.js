const gulpLoadPlugins = require( 'gulp-load-plugins' );
const globals = require( '../globals' );

const $ = gulpLoadPlugins();

module.exports = (gulp) => {

	gulp.task('build:icons', () => {

		return gulp.src(['src/svg-icons/**/*.svg'])
			.pipe($.svgstore())
			.pipe(gulp.dest(`${globals.assetsPath}/svg-icons`));

	});

}
