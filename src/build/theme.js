module.exports = (gulp) => {
	const globals = require( '../globals' );

	gulp.task('build:theme', cb => {
		return gulp.src(
			[
                'src/theme/**/*',
                '!src/theme/acf-json/',
                '!src/theme/acf-json/**'
			], {
				base: './src'
			}
		)
			.pipe(gulp.dest(`${globals.buildPath}`));
	});

	gulp.task('build:theme:plugins', cb => {

		return gulp.src([
				'plugins/**/**',
				'vendor/**/**',
				'wp-content/**/**',
			],{
				base: './'
			}).pipe(gulp.dest(`${globals.buildPath}`));

	});

}
