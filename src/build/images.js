const gulp = require( 'gulp' );
const gulpLoadPlugins = require( 'gulp-load-plugins' );
const globals = require( '../globals' );

const $ = gulpLoadPlugins();
const imagemin = require('gulp-imagemin');
const { development, production } = require('gulp-environments');

module.exports = (gulp) => {

	gulp.task('build:images', () => {

		let stream = gulp.src('src/images/**/*');

		if (production()) {
			stream = stream.pipe(imagemin([
				imagemin.gifsicle({interlaced: true}),
				imagemin.jpegtran({progressive: true}),
				imagemin.optipng({optimizationLevel: 5}),
				imagemin.svgo({
					plugins: [
						{removeViewBox: true},
						{cleanupIDs: false}
					]
				})
			]));
		}

		stream = stream.pipe(gulp.dest(`${globals.assetsPath}/images`));

		return stream;

	});

}
