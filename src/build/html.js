module.exports = (gulp) => {
	const globals = require( '../globals' );

	gulp.task('build:html', cb => {
		return gulp.src(
			[
				'src/html/**/*',
			], {
				base: './src/html'
			}
		)
			.pipe(gulp.dest(globals.buildPath));
	});

	gulp.task('build:html:twig', function () {
		'use strict';
		var twig = require('gulp-twig');
		return gulp.src('./src/html-twig/main/**/*')
			.pipe(twig({
				'base': 'src/html-twig/partials',
			}))
			.pipe(gulp.dest(globals.buildPath));
	});

}
