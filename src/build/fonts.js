module.exports = (gulp) => {
	const globals = require( '../globals' );

	gulp.task('build:fonts', cb => {
		return gulp.src(
			[
				'src/fonts/**/*',
			], {
				base: './src/fonts'
			}
		)
		.pipe(gulp.dest(`${globals.buildPath}/fonts`));
	});

}
