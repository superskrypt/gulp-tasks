const gulpLoadPlugins = require( 'gulp-load-plugins' );
const autoprefixer = require( 'autoprefixer' );
const postcssNano = require('cssnano')
const environments = require( 'gulp-environments' );
const globals = require('../globals');

const touch = require('gulp-touch-cmd');;

const $ = gulpLoadPlugins();
const { development, production } = environments;

module.exports = (gulp) => {
	gulp.task('build:styles', () => {

		return gulp.src('src/scss/*.scss')
			.pipe($.plumber())
			.pipe($.if(development(), $.sourcemaps.init()))
			.pipe($.sass.sync({
				outputStyle: 'expanded',
				precision: 10,
				includePaths: ['node_modules', '.']
			}).on('error', $.sass.logError))
			.pipe($.postcss([
				autoprefixer({ browsers: ['> 1%', 'last 2 versions', 'Firefox ESR'] })
			]))
            .pipe($.if(production(), $.postcss([postcssNano()])))
			.pipe($.if(development(), $.sourcemaps.write()))
			.pipe(gulp.dest(`${globals.assetsPath}/css`))
			.pipe(touch())
			;

	});
}
