const path = require( 'path' );
const webpack = require( 'webpack' );
const es6PromisePromise = require( 'es6-promise-promise' );
const environments = require('gulp-environments');
const glob = require( 'glob' );

const { production, development } = environments;

let fs = require('fs');
let config = JSON.parse(fs.readFileSync('./config.json'));

const webpack_includes = [
	path.resolve('./src/js'),
]

if( config.webpack_includes ) {

	config.webpack_includes.forEach( includePath => {

		webpack_includes.push( path.resolve(includePath) );

	} );

}

const toObject = ( filePaths ) => {

	const ret = {};

	filePaths.forEach( filePath => {

		// you can define entry names mapped to [name] here
		const name = filePath.split( '/' ).slice( -1 )[0];
		ret[name.substring( 0, name.length - 3 )] = filePath;

	} );

	return ret;
};

// This selects our entry points for webpack
const entries = toObject( glob.sync( './src/js/*.js' ) );

const webpackConfig = {
	entry: entries,
	module: {
		loaders: [
			{
			    test: /\.js$/,
				include: webpack_includes,
			    loader: 'babel-loader',
				options: {
					presets: [
						'stage-0',
						'es2015'
					]
				}
			}
		],
	},
	output: {
		filename: '[name].js',
		path: path.resolve( __dirname, 'js' ),
	},
	devtool:'inline-source-map',
	plugins: [
		new webpack.ProvidePlugin( {
			Promise: 'es6-promise-promise',
		} ),
	]
};

module.exports = webpackConfig
