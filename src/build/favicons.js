module.exports = (gulp) => {

    const favicons = require("gulp-favicons");
    const gutil = require("gulp-util");
    const fs = require('fs');
    const packagejson = JSON.parse(fs.readFileSync('./package.json'));
    const globals = require( '../globals' );

    gulp.task('build:favicons', function () {
        return gulp
            .src(`${globals.faviconPath}`)
            .pipe(favicons({
                appName: packagejson.name,
                appDescription: packagejson.description,
                developerName: packagejson.author,
                developerURL: 'https://superskrypt.pl',
                path: `${globals.assetsPath}/images/favicons/`,
                url: null,
                display: 'standalone',
                orientation: 'portrait',
                start_url: '/?homescreen=1',
                version: packagejson.version,
                logging: true,
                online: false,
                icons: {
                    android: false,
                    appleIcon: false,
                    appleStartup: false,
                    favicons: true,
                    firefox: false,
                    windows: false,
                    yandex: false
                },
                html: 'index.html',
                pipeHTML: true,
                replace: true
            }))
            .on('error', gutil.log)
            .pipe(gulp.dest( `${globals.assetsPath}/images/favicons/` ));
    });
}
