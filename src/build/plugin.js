module.exports = (gulp) => {
	const globals = require( '../globals' );

	gulp.task('build:plugin', cb => {
		return gulp.src(
			[
				'src/wp-plugin/**/*',
			], {
				base: './src/wp-plugin'
			}
		)
			.pipe(gulp.dest(`${globals.buildPath}`));
	});

}
