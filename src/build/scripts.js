const gulp = require( 'gulp' );
const gulpLoadPlugins = require( 'gulp-load-plugins' );
const browserSync = require( 'browser-sync' );
const webpackStream = require( 'webpack-stream' );
const webpackConfig = require( './webpack.config' );
const globals = require( '../globals' );

const $ = gulpLoadPlugins();
const { reload } = browserSync;
const { development, production } = require('gulp-environments');
const webpack = require('webpack');
const UglifyJSPlugin = require( 'uglifyjs-webpack-plugin' );

module.exports = (gulp) => {

	gulp.task('build:scripts', done => {

		if (globals.isScriptFileExist) {
			if (production()) {
				webpackConfig.plugins.push(new UglifyJSPlugin());
			}

			return gulp.src('src/js/**/*.js')
				.pipe(webpackStream({
					config : require('./webpack.config.js')
				}))
				.on('error', function handleError() {
					this.emit('end'); // Recover from errors
				})
				.pipe(gulp.dest(`${globals.assetsPath}/js`));

		}

		return done();

	});

	gulp.task('build:sripts:static', done => {
		return gulp.src(
			[
				'src/js/vendors/**/*',
			], {
				base: './src/js/vendors'
			}
		)
			.pipe(gulp.dest(`${globals.assetsPath}/js/vendors`));
	});

}
