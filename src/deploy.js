module.exports = (gulp) => {

    let rsync = require('gulp-rsync');
    let fs = require('fs');
    const prompt = require('gulp-prompt');
    const runSequence = require('gulp4-run-sequence');
    let config = JSON.parse(fs.readFileSync('./config.json'));

    let requireDir = require('require-dir');
    const globals = require('./globals');

    requireDir('./');

    function deployToEnv(dir, destEnv) {
        console.log('deploying ' + dir + ' ' + destEnv.host);
        return gulp.src(dir)
            .pipe(rsync({
                root: dir,
                hostname: destEnv.host,
                port: destEnv.port ? destEnv.port : 22,
                username: destEnv.user,
                destination: `${destEnv.path}`,
                compress: true,
                update: true,
                emptyDirectories: true,
                clean: true,
                recursive: true,
            }));

    }

    gulp.task('deployTst', gulp.series(
        'build',
        () => deployToEnv(globals.buildPath, config.envs.tst),
    ));

    gulp.task('checkIfReleaseNeeded', (cb) => {

        gulp.src('./package.json', { read: false })
            .pipe(prompt.prompt({
                type: 'list',
                name: 'release',
                message: `Create release?`,
                choices: ['yes', 'no'],
            }, (res) => {

                if (res.release == 'yes') {

                    let packagejson = JSON.parse(fs.readFileSync('./package.json'));

                    return runSequence(['release'], cb);

                } else {

                    cb();

                }


            }));

    });

    gulp.task('deployProd', gulp.series(
        'checkIfReleaseNeeded',
        () => {
            let packagejson = JSON.parse(fs.readFileSync('./package.json'));
            deployToEnv(`./release/${packagejson.name}-${packagejson.version}`, config.envs.prod);
        }
    ));

}